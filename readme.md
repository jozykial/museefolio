# MuseeFolio

museefolio est un site internet présentant des peintures à un artiste peintre de publier ses oeuvres.

## Environnement de développement

### Pré-requis

- PHP 7.4
- Composer
- Symfony CLI
- Docker
- Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI symfony):

````bash
symfony check:requierements

### Lancer l'environnement de développement

``` bash
docker-compose up -d
symfony server -d

### Lancer des test

``` bash
php bin/phpunit --testdox
```
````
